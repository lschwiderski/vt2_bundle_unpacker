// Copyright (C) 2019-2021  Lucas Schwiderski
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use std::path::{Path, PathBuf};

use clap::{Args, Subcommand};
use color_eyre::eyre::bail;
use color_eyre::eyre::Context as _;
use color_eyre::Result;
use regex::{Captures, Regex};

use crate::Context;

#[derive(Subcommand, Debug)]
pub enum Operation {
    /// Extract path-like strings from Lua source code
    LuaPaths {
        /// The file to read
        file: PathBuf,
    },
}

/// Run experimental tools
#[derive(Args, Debug)]
pub struct Command {
    #[command(subcommand)]
    operation: Operation,
}

#[tracing::instrument]
fn get_capture<'a>(c: &Captures<'a>, i: usize) -> Result<&'a str> {
    match c.get(i) {
        Some(m) => Ok(m.as_str()),
        None => bail!("invalid data: {:?}", c),
    }
}

#[tracing::instrument]
fn extract_lua_paths(path: impl AsRef<Path> + std::fmt::Debug) -> Result<()> {
    let f = File::open(path.as_ref())?;
    let r = BufReader::new(f);

    let quoted_string_re = Regex::new(r#""([^"]+)""#)?;
    let require_re = Regex::new(r#"(?:^|\s)(?:local_|safe_)?require\("([^"]+)"\)"#)?;
    let base_require_re =
        Regex::new(r#"(?:^|\s)(base|foundation|game|core)_require\("([^"]+)"((?:, "[^"]+")+)\)"#)?;
    let path_re = Regex::new(r#""(\w+(?:/\w+)+)""#)?;

    for (i, result) in r.lines().enumerate() {
        let line = match result.wrap_err_with(|| {
            format!(
                "failed to read line {} in file {}",
                i,
                path.as_ref().display()
            )
        }) {
            Ok(l) => l,
            Err(e) => {
                write!(&mut std::io::stderr(), "{:#?}", e)?;
                continue;
            }
        };

        if let Some(cap) = require_re.captures(&line) {
            println!("{}", get_capture(&cap, 1)?);
            continue;
        }

        if let Some(cap) = path_re.captures(&line) {
            println!("{}", get_capture(&cap, 1)?);
            continue;
        }

        if let Some(cap) = base_require_re.captures(&line) {
            let base = match get_capture(&cap, 1)? {
                "base" | "foundation" => "foundation/scripts",
                "game" => "scripts",
                "core" => "core",
                _ => bail!("Unknown match in base_require_re."),
            };
            let parent = get_capture(&cap, 2)?;
            let files = get_capture(&cap, 3)?;
            for file in quoted_string_re.captures_iter(files) {
                println!("{}/{}/{}", base, parent, get_capture(&file, 1)?);
            }
            continue;
        }
    }

    Ok(())
}

#[tracing::instrument(skip(ctx), fields(%ctx))]
pub fn run(ctx: Context, matches: Command) -> Result<()> {
    match matches.operation {
        Operation::LuaPaths { file: path } => extract_lua_paths(&path)
            .wrap_err_with(|| format!("failed to extract from '{}'", path.display())),
    }
}
