// Copyright (C) 2019-2021  Lucas Schwiderski
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::fs;
use std::path::PathBuf;

use clap::Args;
use color_eyre::eyre::{bail, eyre, Context as _};
use color_eyre::Result;
use glob::Pattern;

use super::util;
use unpacker::{BundleFile, CmdLine, Context};

/// Extract bundle files
#[derive(Args)]
pub struct Command {
    /// Only extract files that match the given glob pattern.
    /// Argument can be specified multiple times.
    #[arg(short, long, value_parser = clap::value_parser!(Pattern))]
    include: Vec<Pattern>,

    /// Do not extract files that match the given glob pattern.
    /// Argument can be specified multiple times.
    #[arg(short, long, value_parser = clap::value_parser!(Pattern))]
    exclude: Vec<Pattern>,

    /// Flatten extracted files into the destination directory.
    /// This will drop any folder structure that might be used in the bundle.
    #[arg(short, long)]
    flatten: bool,

    /// Simulate and log operation, without writing to disk.
    #[arg(short = 'n', long)]
    dry_run: bool,

    /// Attempt to decompile binary data.
    /// This is implemented for only a subset of the file types.
    #[arg(short, long)]
    decompile: bool,

    /// Read the provided bundle without applying patch files.
    /// Can be used to read a patch bundle.
    #[arg(long)]
    skip_patches: bool,

    // TODO: Implement value parser for CmdLine
    /// Path to custom ljd executable.
    /// If not set, 'ljd' will be called from PATH.
    #[arg(long)]
    ljd: Option<CmdLine>,

    // TODO: Implement value parser for CmdLine
    /// Path to custom revorb executable.
    /// If not set, 'revorb' will be called from PATH.
    #[arg(long)]
    revorb: Option<CmdLine>,

    // TODO: Implement value parser for CmdLine
    /// Path to custom ww2ogg executable.
    /// If not set, 'ww2ogg' will be called from PATH.
    /// See project README for how to set up the script.
    #[arg(long)]
    ww2ogg: Option<CmdLine>,

    /// the bundle file to read
    bundle: PathBuf,

    /// The destination folder to write to. Must exist and
    /// be writable.
    folder: PathBuf,
}

// Use a custom impl to be able to cut down some of the fields.
// Particularly the ones containing types from external crates where we don't control their
// individual formatting.
impl std::fmt::Debug for Command {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Command")
            .field("include", &self.include.iter().map(|pattern| pattern.as_str()).collect::<Vec<_>>())
            .field("exclude", &self.exclude.iter().map(|pattern| pattern.as_str()).collect::<Vec<_>>())
            .field("flatten", &self.flatten)
            .field("dry_run", &self.dry_run)
            .field("decompile", &self.decompile)
            .field("skip_patches", &self.skip_patches)
            .field("ljd", &self.ljd)
            .field("revorb", &self.revorb)
            .field("ww2ogg", &self.ww2ogg)
            .field("bundle", &self.bundle)
            .field("folder", &self.folder)
            .finish()
    }
}

impl Command {
    #[inline]
    pub fn get_bundle_path(&self) -> &PathBuf {
        &self.bundle
    }
}

#[tracing::instrument(skip(ctx), fields(%ctx))]
pub fn run(ctx: Context, matches: Command) -> Result<()> {
    let bundle_path = matches.bundle;
    let dest = matches.folder;
    if !dest.is_dir() {
        bail!(
            "destination '{}' doesn't exist or is not a directory",
            dest.display()
        )
    }

    tracing::debug!("Using include patterns: {:?}", matches.include);
    tracing::debug!("Using exclude patterns: {:?}", matches.exclude);

    let patches = util::get_patch_paths(&bundle_path, matches.skip_patches)?;
    let bundle = unpacker::read_bundle(&ctx, &bundle_path, patches)
        .wrap_err_with(|| format!("failed to load bundle '{}'", bundle_path.display()))?;

    let files = bundle
        .get_files()
        .filter(|file| {
            let name = file.get_file_name(None);
            // When there is no `includes`, all files are included
            let is_included = matches.include.is_empty()
                || matches.include.iter().any(|glob| glob.matches(&name));
            // When there is no `excludes`, no file is excluded
            let is_excluded = !matches.exclude.is_empty()
                && matches.exclude.iter().any(|glob| glob.matches(&name));

            is_included && !is_excluded
        })
        .collect::<Vec<&BundleFile>>();

    tracing::debug!("Found {} files to decompile", files.len());

    for file in files {
        let (decompiled, name) = if matches.decompile {
            (file.get_decompiled(&ctx), file.get_decompiled_name(None))
        } else {
            (file.get_raw(), file.get_file_name(None))
        };

        match decompiled {
            Ok(files) => {
                match files.len() {
                    0 => {
                        tracing::warn!(
                            "Decompilation did not produce any data for file {}",
                            file.get_decompiled_name(None)
                        );
                    }
                    // For a single file we want to use the bundle file's name.
                    1 => {
                        // We already checked `files.len()`.
                        let file = files.first().unwrap();

                        let name = file.get_name().unwrap_or(&name);
                        let name = if matches.flatten {
                            name.replace('/', "_")
                        } else {
                            name.clone()
                        };

                        let mut path = dest.clone();
                        path.push(name);

                        if tracing::enabled!(tracing::Level::DEBUG) || matches.dry_run {
                            tracing::info!("Writing file '{}'.", path.display());
                        }

                        if !matches.dry_run {
                            if let Some(parent) = path.parent() {
                                fs::create_dir_all(parent)?;
                            }
                            fs::write(&path, file.get_data())?;
                        }
                    }
                    // For multiple files we create a directory and name files
                    // by index.
                    _ => {
                        for (i, file) in files.iter().enumerate() {
                            let mut path = dest.clone();

                            let name = file
                                .get_name()
                                .map(|name| {
                                    if matches.flatten {
                                        name.replace('/', "_")
                                    } else {
                                        name.clone()
                                    }
                                })
                                .unwrap_or(format!("{}", i));

                            path.push(name);

                            if tracing::enabled!(tracing::Level::DEBUG) || matches.dry_run {
                                tracing::info!("Writing file '{}'.", path.display());
                            }

                            if !matches.dry_run {
                                let parent = path.parent().ok_or_else(|| {
                                    eyre!(
                                        "Decompilation produced invalid file path: {}",
                                        path.display()
                                    )
                                })?;
                                // Make sure to create any sub-directory layout that decompilers specify.
                                // Certain file types (e.g. `.wwise_bank`) use this.
                                fs::create_dir_all(parent)?;
                                fs::write(path, file.get_data())?;
                            }
                        }
                    }
                }
            }
            Err(err) => {
                let name = if matches.decompile {
                    file.get_decompiled_name(None)
                } else {
                    file.get_file_name(None)
                };

                let err = err.wrap_err(format!("Failed to decompile '{}'", name));

                tracing::error!("{:?}", err);
                // TODO: Allow stopping on error
            }
        };
    }

    Ok(())
}
