// Copyright (C) 2019-2021  Lucas Schwiderski
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
use clap::Args;
use clap::Subcommand;
use std::fs;
use std::fs::File;
use std::fs::OpenOptions;
use std::io::prelude::*;
use std::io::BufReader;
use std::io::BufWriter;
use std::path::PathBuf;
use unpacker::murmur::Murmur32;
use unpacker::murmur::Murmur64;
use unpacker::murmur::SEED;

use color_eyre::eyre::bail;
use color_eyre::Result;

use unpacker::murmur;
use unpacker::murmur::HashGroup;
use unpacker::Context;

#[derive(Subcommand, Debug)]
pub enum Operation {
    /// Create a Murmur hash from the given input
    Hash {
        /// Use the 32-bit hash algorithm
        /// Note that this is the engine's fake 32-bit implementation, whereby
        /// a 64-bit hash is calculate, but then the upper half is used as a 32-bit
        /// hash.
        #[arg(long = "32")]
        fake32: bool,

        input: String,
    },
    /// Look up a hash in the dictionary (see global '--dict' flag)
    Lookup {
        /// The group to check
        #[arg(short, long)]
        group: HashGroup,

        /// Use a 32-bit hash
        #[arg(long = "32")]
        fake32: bool,

        input: String,
    },
    /// Create the inverse hash for the given input.
    /// An inverse hash is a mathematically calculated value that, when passed through the
    /// same Murmur hash function, creates the same initial hash.
    Inverse {
        /// Use the 32-bit hash algorithm
        /// Note that this is the engine's fake 32-bit implementation, whereby
        /// a 64-bit hash is calculate, but then the upper half is used as a 32-bit
        /// hash.
        #[arg(long = "32")]
        fake32: bool,

        hash: String,
    },
    /// Add a list of values to a dictionary
    Add {
        /// The group to assign these values to
        #[arg(short, long)]
        group: HashGroup,

        /// Path to a file containing strings line by line.
        values: PathBuf,

        /// Path to the dictionary file to write to.
        /// Will be created if it doesn't exist.
        dictionary: PathBuf,
    },
    /// Experimental
    Brute {
        #[arg(long)]
        initial_value: Option<String>,

        /// Use the 32-bit hash algorithm
        /// Note that this is the engine's fake 32-bit implementation, whereby
        /// a 64-bit hash is calculate, but then the upper half is used as a 32-bit
        /// hash.
        #[arg(long = "32")]
        fake32: bool,

        hashes: PathBuf,
    },
    /// Experimental
    BruteTerms {
        /// Use the 32-bit hash algorithm
        /// Note that this is the engine's fake 32-bit implementation, whereby
        /// a 64-bit hash is calculate, but then the upper half is used as a 32-bit
        /// hash.
        #[arg(long = "32")]
        fake32: bool,

        terms: PathBuf,
        hashes: PathBuf,
    },
}

/// Run tools regarding Murmur hashes
#[derive(Args, Debug)]
pub struct Command {
    #[command(subcommand)]
    operation: Operation,
}

fn increment_string<T>(s: &mut String, alphabet: T)
where
    T: AsRef<[char]>,
{
    match s.pop() {
        Some(c) => {
            let alphabet = alphabet.as_ref();
            let i = alphabet.iter().position(|&a| a == c).unwrap();

            if i == alphabet.len() - 1 {
                increment_string(s, alphabet);
                s.push(alphabet[0]);
            } else {
                s.push(alphabet[i + 1]);
            }
        }
        None => s.push(alphabet.as_ref()[0]),
    }
}

#[tracing::instrument(skip(ctx), fields(%ctx))]
pub fn run(ctx: Context, matches: Command) -> Result<()> {
    let is_tty = atty::is(atty::Stream::Stdout);

    match matches.operation {
        Operation::Hash { fake32, input } => {
            let output = if fake32 {
                format!("{:08X}", murmur::hash32(input.as_bytes(), murmur::SEED))
            } else {
                format!(
                    "{:016X}",
                    murmur::hash(input.as_bytes(), murmur::SEED as u64)
                )
            };

            if is_tty {
                println!("{}", output);
            } else {
                print!("{}", output);
            }
        }
        Operation::Lookup {
            fake32,
            group,
            input,
        } => {
            let output = if fake32 {
                let hash = match u32::from_str_radix(&input, 16) {
                    Ok(h) => h,
                    Err(e) => bail!("Input is not a valid hex sequence: {}", e),
                };
                match ctx.dictionary.lookup_short(hash.into(), group) {
                    Some(s) => s.clone(),
                    None => format!("Unknown hash {}", input),
                }
            } else {
                let hash = match u64::from_str_radix(&input, 16) {
                    Ok(h) => h,
                    Err(e) => bail!("Input is not a valid hex sequence: {}", e),
                };
                match ctx.dictionary.lookup(hash.into(), group) {
                    Some(s) => s.clone(),
                    None => format!("Unknown hash {}", input),
                }
            };

            if is_tty {
                println!("{}", output);
            } else {
                print!("{}", output);
            }
        }
        Operation::Inverse { fake32, hash } => {
            let output = if fake32 {
                let hash = match u32::from_str_radix(&hash, 16) {
                    Ok(h) => h,
                    Err(e) => bail!("Input is not a valid hex sequence: {}", e),
                };

                tracing::debug!("Calculating inverse of {:08X}", hash);

                format!("{:08X}", murmur::inverse32(hash, murmur::SEED))
            } else {
                let hash = match u64::from_str_radix(&hash, 16) {
                    Ok(h) => h,
                    Err(e) => bail!("Input is not a valid hex sequence: {}", e),
                };

                tracing::debug!("Calculating inverse of {:016X}", hash);

                format!("{:016X}", murmur::inverse(hash, murmur::SEED as u64))
            };

            if is_tty {
                println!("{}", output);
            } else {
                print!("{}", output);
            }
        }
        Operation::Add {
            group,
            values,
            dictionary,
        } => {
            tracing::debug!(
                "Adding strings from '{}' to '{}'",
                values.display(),
                dictionary.display()
            );

            let in_file = File::open(values)?;
            let reader = BufReader::new(in_file);

            let mut dict = ctx.dictionary.clone();

            let mut added = 0;
            let mut skipped = 0;

            for line in reader.lines() {
                let line = line?.trim().to_string();

                if dict.has(&line, group) {
                    skipped += 1;
                    continue;
                }

                let long = Murmur64::from(murmur::hash(line.as_bytes(), SEED as u64));
                let short = Murmur32::from(murmur::hash32(line.as_bytes(), SEED));
                dict.add(line, long, short, group);
                added += 1;
            }

            let total = dict.len();

            let out_file = OpenOptions::new()
                .write(true)
                .create(true)
                .truncate(true)
                .open(dictionary)?;
            let mut writer = BufWriter::new(out_file);
            dict.write_csv(&mut writer)?;
            writer.flush()?;

            tracing::info!(
                "Added {} entries, skipped {} duplicates. Total now {}",
                added,
                skipped,
                total
            );
        }
        Operation::Brute {
            fake32,
            initial_value,
            hashes,
        } => {
            let hashes = fs::read_to_string(hashes)?;
            let hashes: Vec<u64> = hashes
                .lines()
                .map(|hash| u64::from_str_radix(hash, 16).map_err(|e| e.into()))
                .collect::<Result<Vec<u64>>>()?;

            let alphabet = vec![
                'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p',
                'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '_',
            ];
            let mut needle = initial_value.unwrap_or_default();

            let mut i: u64 = 0;
            loop {
                increment_string(&mut needle, &alphabet);

                i = i.wrapping_add(1);
                if i % 300000 == 0 {
                    eprint!("\r{}", needle);
                }

                let h = if fake32 {
                    murmur::hash(needle.as_bytes(), murmur::SEED as u64) >> 32
                } else {
                    murmur::hash(needle.as_bytes(), murmur::SEED as u64)
                };

                for hash in &hashes {
                    if h == *hash {
                        println!("\r{:08X} -> {}", hash, needle);
                    }
                }
            }
        }
        Operation::BruteTerms {
            fake32,
            terms,
            hashes,
        } => {
            let terms = fs::read_to_string(terms)?;
            let hashes = fs::read_to_string(hashes)?;

            let hashes: Vec<u64> = hashes
                .lines()
                .map(|hash| u64::from_str_radix(hash, 16).map_err(|e| e.into()))
                .collect::<Result<Vec<u64>>>()?;

            let mut i: u64 = 0;
            for term in terms.lines() {
                for term2 in terms.lines() {
                    let needle = format!("{}_{}", term, term2);

                    let h = if fake32 {
                        murmur::hash(needle.as_bytes(), murmur::SEED as u64) >> 32
                    } else {
                        murmur::hash(needle.as_bytes(), murmur::SEED as u64)
                    };

                    for hash in &hashes {
                        if h == *hash {
                            println!("\r{:08X} -> {}", hash, needle);
                        }
                    }

                    for term3 in terms.lines() {
                        let needle = format!("{}_{}_{}", term, term2, term3);

                        i = i.wrapping_add(1);
                        if i % 300000 == 0 {
                            eprint!("\r{}\r{}", " ".repeat(80), needle);
                        }

                        let h = if fake32 {
                            murmur::hash(needle.as_bytes(), murmur::SEED as u64) >> 32
                        } else {
                            murmur::hash(needle.as_bytes(), murmur::SEED as u64)
                        };

                        for hash in &hashes {
                            if h == *hash {
                                println!("\r{:08X} -> {}", hash, needle);
                            }
                        }
                    }
                }
            }
        }
    }

    Ok(())
}
