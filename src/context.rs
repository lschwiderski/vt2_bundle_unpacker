// Copyright (C) 2019-2021  Lucas Schwiderski
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
use std::ffi::{OsStr, OsString};
use std::path::PathBuf;
use std::process::Command;

use crate::murmur::Dictionary;

#[derive(Clone)]
pub struct CmdLine {
    cmd: OsString,
    args: Vec<OsString>,
}

impl CmdLine {
    pub fn new(cmd: impl Into<OsString>) -> Self {
        Self {
            cmd: cmd.into(),
            args: vec![],
        }
    }

    pub fn arg(&mut self, arg: impl Into<OsString>) -> &mut Self {
        self.args.push(arg.into());
        self
    }
}

impl std::fmt::Debug for CmdLine {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("CmdLine")
            .field("cmd", &self.cmd)
            .field("args", &self.args)
            .finish()
    }
}

impl std::fmt::Display for CmdLine {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "\"{}\"", self.cmd.to_string_lossy())?;

        for arg in &self.args {
            write!(f, " \"{}\"", arg.to_string_lossy())?;
        }

        Ok(())
    }
}

impl From<&CmdLine> for Command {
    fn from(value: &CmdLine) -> Self {
        let mut cmd = Command::new(&value.cmd);
        cmd.args(&value.args);
        cmd
    }
}

impl std::str::FromStr for CmdLine {
    type Err = String;

    #[tracing::instrument]
    fn from_str(tmpl: &str) -> std::result::Result<Self, Self::Err> {
        if tmpl.trim().is_empty() {
            return Err("Command line template must not be empty".into());
        }

        let path = PathBuf::from(tmpl);
        let cmd = if path.exists() {
            if path.file_name() == Some(OsStr::new("main.py")) {
                let arg = path.display().to_string();
                let mut cmd = CmdLine::new("python");
                let quoted = shlex::try_quote(&arg).map_err(|err| format!("{}", err))?;
                cmd.arg(quoted.to_string());
                cmd
            } else {
                CmdLine::new(path)
            }
        } else {
            let Some(args) = shlex::split(tmpl) else {
                return Err("Invalid shell syntax".into());
            };

            // We already checked that the template is not empty
            let mut cmd = CmdLine::new(args[0].clone());
            let mut it = args.iter();
            // Skip the first one, that's the command name
            it.next();

            for arg in it {
                cmd.arg(arg);
            }

            cmd
        };

        Ok(cmd)
    }
}

pub struct Context {
    pub dictionary: Dictionary,
    pub verbosity: u8,
    pub compression_dictionary: Option<PathBuf>,
    pub ljd: Option<CmdLine>,
    pub revorb: Option<CmdLine>,
    pub ww2ogg: Option<CmdLine>,
}

impl std::fmt::Display for Context {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Context")
            .field(
                "dictionary",
                &format!("Dictionary({} entries)", self.dictionary.len()),
            )
            .field("verbosity", &self.verbosity)
            .field("compression_dictionary", &self.compression_dictionary)
            .field(
                "ljd",
                &match &self.ljd {
                    Some(cmd) => format!("Some({})", cmd),
                    None => "None".into(),
                },
            )
            .field(
                "revorb",
                &match &self.revorb {
                    Some(cmd) => format!("Some({})", cmd),
                    None => "None".into(),
                },
            )
            .field(
                "ww2ogg",
                &match &self.ww2ogg {
                    Some(cmd) => format!("Some({})", cmd),
                    None => "None".into(),
                },
            )
            .finish()
    }
}
