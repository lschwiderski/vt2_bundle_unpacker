use std::io::{BufRead, Write};
use std::str::FromStr as _;

use clap::ValueEnum;
use color_eyre::eyre::{Context as _, OptionExt as _};
use color_eyre::Result;
use serde::{Deserialize, Serialize};

use super::{murmurhash64, Murmur32, Murmur64, SEED};

type HashMap<K, V> = rustc_hash::FxHashMap<K, V>;

#[derive(Copy, Clone, Debug, Deserialize, PartialEq, Eq, Hash, Serialize, ValueEnum)]
#[serde(rename_all = "snake_case")]
pub enum HashGroup {
    Filename,
    Filetype,
    Strings,
    Material,
    NetworkConfig,
    Other,
}

impl HashGroup {
    pub fn all() -> [Self; 6] {
        [
            Self::Filename,
            Self::Filetype,
            Self::Other,
            Self::Strings,
            Self::Material,
            Self::NetworkConfig,
        ]
    }
}

impl std::fmt::Display for HashGroup {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Filename => write!(f, "filename"),
            Self::Filetype => write!(f, "filetype"),
            Self::Strings => write!(f, "strings"),
            Self::Material => write!(f, "material"),
            Self::NetworkConfig => write!(f, "network_config"),
            Self::Other => write!(f, "other"),
        }
    }
}

impl std::str::FromStr for HashGroup {
    type Err = color_eyre::Report;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "filename" => Ok(Self::Filename),
            "filetype" => Ok(Self::Filetype),
            "strings" => Ok(Self::Strings),
            "material" => Ok(Self::Material),
            "network_config" => Ok(Self::NetworkConfig),
            "other" => Ok(Self::Other),
            _ => color_eyre::eyre::bail!("Invalid hash group: {}", s),
        }
    }
}

impl Default for HashGroup {
    fn default() -> Self {
        Self::Other
    }
}

#[derive(Deserialize, Serialize)]
struct Row {
    // NOTE: The order of fields is important, as the CSV serializer copies that.
    value: String,
    long: Option<Murmur64>,
    short: Option<Murmur32>,
    #[serde(default)]
    group: HashGroup,
}

#[derive(Debug, Clone)]
pub struct Dictionary {
    long: HashMap<Murmur64, HashMap<HashGroup, String>>,
    short: HashMap<Murmur32, HashMap<HashGroup, String>>,
}

impl Default for Dictionary {
    fn default() -> Self {
        Self::new()
    }
}

impl Dictionary {
    pub fn new() -> Self {
        Self {
            long: HashMap::default(),
            short: HashMap::default(),
        }
    }

    #[tracing::instrument(skip(r))]
    pub fn read_seed_file<R: BufRead>(&mut self, r: R) -> Result<()> {
        for line in r.lines() {
            let line = line?;

            let long = Murmur64::from(murmurhash64::hash(line.as_bytes(), SEED as u64));
            let short = Murmur32::from(murmurhash64::hash32(line.as_bytes(), SEED));

            // In the legacy dictionary, there were no groups.
            // Instead, separate dictionary files would be specified
            // based on the file types that were processed.
            // To allow that to work, still, we need to add the line
            // to every group that existed at the time.
            self.add(line.clone(), long, short, HashGroup::Filename);
            self.add(line.clone(), long, short, HashGroup::Filetype);
            self.add(line, long, short, HashGroup::Other);
        }

        Ok(())
    }

    #[tracing::instrument(skip(r))]
    pub fn read_csv(&mut self, r: impl BufRead) -> Result<()> {
        let mut first = true;

        for (i, line) in r.lines().enumerate() {
            // Skip the header line
            if first {
                first = false;
                continue;
            }

            let line = line?;
            let mut split = line.split(',');

            let value = split
                .next()
                .map(|s| s.to_string())
                .ok_or_eyre("Invalid dictionary line, missing string value")
                .wrap_err_with(|| format!("line {}: {}", i, line))?;
            let long = split
                .next()
                .ok_or_eyre("Missing long hash")
                .and_then(Murmur64::from_str)
                .wrap_err("Invalid dictionary line, invalid long hash")
                .wrap_err_with(|| format!("line {}: {}", i, line))?;
            let short = split
                .next()
                .ok_or_eyre("Missing short hash")
                .and_then(Murmur32::from_str)
                .wrap_err("Invalid dictionary line, invalid short hash")
                .wrap_err_with(|| format!("line {}: {}", i, line))?;
            let group = split
                .next()
                .ok_or_eyre("Missing group")
                .and_then(<HashGroup as std::str::FromStr>::from_str)
                .wrap_err("Invalid dictionary line, invalid group")
                .wrap_err_with(|| format!("line {}: {}", i, line))?;

            self.add(value, long, short, group);
        }

        Ok(())
    }

    #[tracing::instrument(skip(w))]
    pub fn write_csv(&self, mut w: impl Write) -> Result<()> {
        writeln!(w, "value,long,short,group")?;

        for (long, v) in self.long.iter() {
            for (group, value) in v {
                writeln!(
                    w,
                    "{},{:016},{:08},{}",
                    value,
                    long,
                    Murmur32::from(*long),
                    group
                )
                .wrap_err("Failed to serialize entry")?
            }
        }

        Ok(())
    }

    #[inline]
    pub fn add(&mut self, value: String, long: Murmur64, short: Murmur32, group: HashGroup) {
        let e = self.long.entry(long).or_default();
        e.insert(group, value.clone());

        let e = self.short.entry(short).or_default();
        e.insert(group, value);
    }

    #[inline]
    pub fn lookup(&self, hash: Murmur64, group: HashGroup) -> Option<&String> {
        let groups = self.long.get(&hash)?;

        if let Some(value) = groups.get(&group) {
            return Some(value);
        }

        if let Some(e) = groups.iter().next() {
            tracing::warn!(
                "Found lookup '{}' for hash {:016X} in group '{}' instead of '{}'",
                e.1,
                hash,
                e.0,
                group
            );

            Some(e.1)
        } else {
            None
        }
    }

    #[inline]
    pub fn lookup_short(&self, hash: Murmur32, group: HashGroup) -> Option<&String> {
        let groups = self.short.get(&hash)?;

        if let Some(value) = groups.get(&group) {
            return Some(value);
        }

        if let Some(e) = groups.iter().next() {
            tracing::warn!(
                "Found lookup '{}' for hash {:08X} in group '{}' instead of '{}'",
                e.1,
                hash,
                e.0,
                group
            );

            Some(e.1)
        } else {
            None
        }
    }

    // This assumes that all entries will always have both a long and a short hash,
    // and therefore checking only one index is enough.
    #[inline]
    pub fn has(&mut self, value: &String, group: HashGroup) -> bool {
        let hash = Murmur64::from(murmurhash64::hash(value.as_bytes(), SEED as u64));
        if let Some(e) = self.long.get(&hash) {
            e.get(&group) == Some(value)
        } else {
            false
        }
    }

    #[inline]
    pub fn len(&self) -> usize {
        self.long.values().fold(0, |sum, groups| sum + groups.len())
    }

    // This assumes that all entries will always have both a long and a short hash,
    // and therefore checking only one index is enough.
    #[inline]
    pub fn is_empty(&self) -> bool {
        self.long.is_empty()
    }
}
