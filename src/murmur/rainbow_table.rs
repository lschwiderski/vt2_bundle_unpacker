// Copyright (C) 2019-2021  Lucas Schwiderski
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
use std::collections::HashMap;

pub struct RainbowTable {
    map: HashMap<u64, String>,
    short_hash_map: HashMap<u32, String>,
}

impl Default for RainbowTable {
    fn default() -> RainbowTable {
        Self::new()
    }
}

impl RainbowTable {
    pub fn new() -> RainbowTable {
        RainbowTable {
            map: HashMap::new(),
            short_hash_map: HashMap::new(),
        }
    }

    pub fn add(&mut self, hash: u64, s: String) {
        self.map.insert(hash, s.clone());
        self.short_hash_map.insert((hash >> 32) as u32, s);
    }

    pub fn lookup(&self, hash: &u64) -> Option<String> {
        match self.map.get(hash) {
            Some(s) => Some(s.clone()),
            // If the full hash didn't return anything, we also try the short hash
            None => self.lookup_short(&((hash >> 32) as u32)),
        }
    }

    pub fn lookup_short(&self, hash: &u32) -> Option<String> {
        self.short_hash_map.get(hash).cloned()
    }
}
