use std::io::{self, Read};

use flate2::read::ZlibDecoder;
use zstd::bulk::Decompressor;

const COMPRESSION_CHUNK_SIZE: usize = 65536;

pub enum BundleDecompressor<'a> {
    Zlib,
    Zstd(Decompressor<'a>),
}

impl<'a> BundleDecompressor<'a> {
    pub fn decompress(&mut self, compressed: impl AsRef<[u8]>) -> io::Result<Vec<u8>> {
        match self {
            BundleDecompressor::Zlib => {
                let mut decoder = ZlibDecoder::new(compressed.as_ref());
                let mut decompressed: Vec<u8> = Vec::with_capacity(COMPRESSION_CHUNK_SIZE);
                decoder.read_to_end(&mut decompressed)?;
                Ok(decompressed)
            }
            BundleDecompressor::Zstd(decompressor) => {
                decompressor.decompress(compressed.as_ref(), COMPRESSION_CHUNK_SIZE)
            }
        }
    }
}
