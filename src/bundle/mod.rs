// Copyright (C) 2019-2021  Lucas Schwiderski
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
use std::collections::{hash_map::Values, HashMap};
use std::fmt::Debug;
use std::fs;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use std::io::{Cursor, ErrorKind, Read, SeekFrom};
use std::path::{Path, PathBuf};

use byteorder::{LittleEndian, ReadBytesExt};
use color_eyre::eyre::{bail, ensure, eyre, Context as _};
use color_eyre::{Report, Result};
use zstd::bulk::Decompressor;

use crate::bundle::decompressor::BundleDecompressor;
use crate::context::Context;
use crate::murmur::HashGroup;
use bundle_file::BundleFileVersion;
pub use bundle_file::UserFile;
pub use bundle_file::{BundleFile, BundleFileType};

mod bundle_file;
mod decompile;
mod decompressor;

#[derive(Copy, Clone, Debug)]
pub enum BundleFormat {
    VT1,
    VT2,
    VT2X,
    VT2_2023,
}

impl TryFrom<u32> for BundleFormat {
    type Error = Report;

    fn try_from(value: u32) -> std::result::Result<Self, Self::Error> {
        match value {
            0xF0000004 => Ok(BundleFormat::VT1),
            0xF0000005 => Ok(BundleFormat::VT2),
            0xF0000006 => Ok(BundleFormat::VT2X),
            0xF0000007 => Ok(BundleFormat::VT2_2023),
            _ => bail!("Invalid bundle format: {:08X}", value),
        }
    }
}

const COMPRESSED_PADDING_SIZE: i64 = 4;

pub type Murmur64 = u64;

type BundleFileMeta = (BundleFileType, Murmur64);

pub struct Bundle {
    files: HashMap<String, BundleFile>,
}

impl Bundle {
    pub fn get_files(&self) -> Values<'_, String, BundleFile> {
        self.files.values()
    }
}

#[tracing::instrument(skip(ctx, r), fields(%ctx))]
pub fn decompress<T>(ctx: &Context, r: &mut T, bundle_format: BundleFormat) -> Result<Vec<u8>>
where
    T: Read + Seek,
{
    let mut data: Vec<u8> = Vec::new();
    let inflate_size = read32!(r)? as usize;

    r.seek(SeekFrom::Current(COMPRESSED_PADDING_SIZE))?;

    let mut decompressor = match bundle_format {
        BundleFormat::VT1 | BundleFormat::VT2 | BundleFormat::VT2X => BundleDecompressor::Zlib,
        BundleFormat::VT2_2023 => {
            let dict = {
                let Some(path) = &ctx.compression_dictionary else {
                    bail!("Couldn't find path to compression dictionary. Please specify with '--zstd-dict'.");
                };
                fs::read(path).wrap_err_with(|| {
                    format!("failed to read compression dictionary: {}", path.display())
                })?
            };
            let decoder = Decompressor::with_dictionary(&dict)
                .wrap_err("failed to create zstd decompressor")?;
            BundleDecompressor::Zstd(decoder)
        }
    };

    loop {
        let block_size = match read32!(r) {
            Ok(size) => size as usize,
            Err(e) => match e.kind() {
                // Reached last block in previous iteration
                ErrorKind::UnexpectedEof => break,
                _ => return Err(e.into()),
            },
        };

        // This specific block size seems to represent a NULL block
        if block_size == 0x10000 {
            r.seek(SeekFrom::Current(block_size as i64))?;
            continue;
        }

        let mut compressed: Vec<u8> = vec![0u8; block_size];
        r.read_exact(&mut compressed)
            .wrap_err_with(|| format!("failed to read block of size {}", block_size))?;

        let mut decompressed = decompressor
            .decompress(compressed)
            .wrap_err("failed to decompress chunk")?;

        data.append(&mut decompressed);
    }

    // Zlib always writes the full decompressed buffer, even if the last block didn't fill the
    // whole buffer.
    // We use `inflate_size` to remove the extraneous data.
    data.truncate(inflate_size);
    Ok(data)
}

#[tracing::instrument(skip(ctx), fields(%ctx))]
fn read<P>(ctx: &Context, path: P) -> Result<Vec<BundleFile>>
where
    P: AsRef<Path> + std::fmt::Debug,
{
    let mut f = BufReader::new(File::open(&path)?);
    let format = BundleFormat::try_from(read32!(f)?)?;
    let data = decompress(ctx, &mut f, format)
        .wrap_err_with(|| format!("failed to decompress '{}'", path.as_ref().display()))?;

    let mut stream_file = {
        let path = path.as_ref();
        path.parent()
            .and_then(|p| {
                path.file_name().and_then(|name| name.to_str()).map(|name| {
                    let mut p = PathBuf::from(p);
                    p.push(format!("{}.stream", name));
                    p
                })
            })
            .and_then(|p| File::open(p).ok())
            .map(BufReader::new)
    };

    if stream_file.is_none() {
        tracing::debug!(
            "No stream file found for bundle {}",
            path.as_ref().display()
        );
    }

    let mut cursor = Cursor::new(data);
    let file_count = read32!(cursor)? as usize;

    // Skip rest of header for now
    // TODO: Check the skipped stuff
    cursor.seek(SeekFrom::Current(256))?;
    let mut files_meta: Vec<BundleFileMeta> = Vec::with_capacity(file_count);

    // First iteration: get list of files
    for _i in 0..file_count {
        let t = read64!(cursor)?;
        let hash = read64!(cursor)?;

        match format {
            BundleFormat::VT2 => {
                cursor.seek(SeekFrom::Current(4))?;
            }
            BundleFormat::VT2X | BundleFormat::VT2_2023 => {
                let unknown = read32!(cursor)?;
                if unknown != 0 {
                    tracing::warn!("Unexpected non-zero value: {} ({:08X})", unknown, unknown);
                }

                // Contains size of data for all file versions combined
                let _data_size = read32!(cursor)?;
            }
            _ => {
                bail!("don't know how much to skip for format {:?}", format);
            }
        };

        files_meta.push((BundleFileType::from(t), hash));
    }

    let mut files: Vec<BundleFile> = Vec::with_capacity(file_count);
    for i in 0..file_count {
        let file_meta = files_meta
            .get(i)
            .ok_or_else(|| eyre!("couldn't find file metadata {} on second iteration", i))?;
        let next_file_meta = files_meta.get(i + 1);

        let bundle_name = ctx
            .dictionary
            .lookup(file_meta.1.into(), HashGroup::Filename)
            .cloned()
            .unwrap_or_else(|| format!("{:016X}", file_meta.1));
        let mut file = BundleFile::new(file_meta.0, file_meta.1, bundle_name);
        let versions = read_file_data(
            ctx,
            &mut cursor,
            &mut stream_file,
            file_meta,
            next_file_meta,
        )
        .wrap_err_with(|| {
            format!(
                "failed to read content for file {}",
                file.get_decompiled_name(None)
            )
        })?;

        file.set_versions(versions);
        files.push(file)
    }

    Ok(files)
}

#[tracing::instrument(skip(ctx), fields(%ctx))]
pub fn read_bundle<P, Q>(ctx: &Context, path: P, patches: Vec<Q>) -> Result<Bundle>
where
    P: AsRef<Path> + Copy + std::fmt::Debug,
    Q: AsRef<Path> + std::fmt::Debug,
{
    let mut file_map: HashMap<String, BundleFile> = HashMap::new();
    let files = read(ctx, path)
        .wrap_err_with(|| format!("failed to read file '{}'", path.as_ref().display()))?;

    for file in files {
        file_map.insert(file.get_file_name(None), file);
    }

    for path in patches {
        let files = read(ctx, &path)
            .wrap_err_with(|| format!("failed to read file '{}'", &path.as_ref().display()))?;
        for file in files {
            file_map.insert(file.get_file_name(None), file);
        }
    }

    Ok(Bundle { files: file_map })
}

#[tracing::instrument(skip(ctx, cursor, stream_cursor), fields(%ctx))]
fn read_file_data<F, G>(
    ctx: &Context,
    cursor: &mut F,
    stream_cursor: &mut Option<G>,
    file_meta: &BundleFileMeta,
    next_file_meta: Option<&BundleFileMeta>,
) -> Result<Vec<BundleFileVersion>>
where
    F: Read + Seek,
    G: Read + Seek,
{
    let t = read64!(cursor)?;
    let name_hash = read64!(cursor)?;

    ensure!(
        file_meta.0 == BundleFileType::from(t),
        "file type mismatch. expected '{:?}', got '{:08X}'",
        file_meta.0,
        t
    );
    ensure!(
        file_meta.1 == name_hash,
        "file name mismatch. expected '{:016X}', got '{:016X}'",
        file_meta.1,
        name_hash
    );

    let version_count = read32!(cursor)? as usize;
    let stream_offset = read32!(cursor)? as u64;

    let mut versions: Vec<BundleFileVersion> = Vec::new();

    // For certain file types the size field is incorrect.
    // The actual size always seems a few thousand bytes off, but in a (seemingly) non-deterministic way.
    // So instead we slowly read from the file until we find the next file by its type hash.
    if file_meta.0 == BundleFileType::WwiseBank || file_meta.0 == BundleFileType::Tome {
        if version_count == 0 {
            tracing::warn!(
                "[Workaround File Length] File has no versions. Skipping, but don't know if that's the way to go."
            );

            return Ok(Vec::new());
        } else if version_count > 1 {
            bail!(
                "Found a {} with a version count of {}! Don't know how to handle this one!",
                file_meta.0.ext_name(),
                version_count,
            );
        }

        tracing::warn!(
            "Found {} file with inconsistent data size. Attempting to compensate.",
            file_meta.0.ext_name()
        );

        let lang = read32!(cursor)?;
        let expected_size = read32!(cursor)? as usize;
        let stream_size = read32!(cursor)? as usize;

        let mut data: Vec<u8> = Vec::new();
        let stream_data: Vec<u8> = if let Some(stream_cursor) = stream_cursor.as_mut() {
            stream_cursor.seek(SeekFrom::Start(stream_offset))?;

            let mut buf = vec![0u8; stream_size];
            stream_cursor
                .read_exact(&mut buf)
                .wrap_err("Failed to read stream data")?;

            buf
        } else {
            Vec::new()
        };

        // Get the type hash of the file that comes after this.
        // Since we don't know the exact size of the current file,
        // we have to read data until the next file starts.
        // If this happens to be the last file, we read until the end.
        match next_file_meta {
            Some(next) => {
                let next_type_hash = u64::from(next.0);
                loop {
                    let peek = read64!(cursor)?;

                    if peek == next_type_hash {
                        cursor.seek(SeekFrom::Current(-8))?;
                        break;
                    } else {
                        data.push(peek.to_le_bytes()[0]);
                        cursor.seek(SeekFrom::Current(-7))?;
                        continue;
                    }
                }
            }
            None => {
                cursor.read_to_end(&mut data)?;
            }
        }

        tracing::warn!(
            "[Workaround File Length] Size of {} should have been {} ({:08X}), but read {} ({:08X}) instead.",
            file_meta.0.ext_name(),
            expected_size,
            expected_size,
            data.len(),
            data.len()
        );

        versions.push(BundleFileVersion {
            lang,
            data,
            stream_data,
        });
    } else {
        // First iteration: read definition
        for _ in 0..version_count {
            let lang = read32!(cursor)?;
            let size = read32!(cursor)? as usize;
            let stream_size = read32!(cursor)? as usize;

            let stream_data: Vec<u8> = if let Some(stream_cursor) = stream_cursor.as_mut() {
                stream_cursor.seek(SeekFrom::Start(stream_offset))?;

                let mut buf = vec![0u8; stream_size];
                stream_cursor
                    .read_exact(&mut buf)
                    .wrap_err("Failed to read from stream data")?;

                buf
            } else {
                Vec::new()
            };

            versions.push(BundleFileVersion {
                lang,
                data: vec![0u8; size],
                stream_data,
            });
        }

        // Second iteration: read content
        for j in 0..version_count {
            let version = versions
                .get_mut(j)
                .ok_or_else(|| eyre!("couldn't find version {} on second iteration", j))?;

            cursor.read_exact(&mut version.data)?;
        }
    }

    Ok(versions)
}
