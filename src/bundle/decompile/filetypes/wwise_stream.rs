// Copyright (C) 2019-2021  Lucas Schwiderski
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
use std::env;
use std::fs;
use std::io::{Cursor, Seek, SeekFrom};
use std::process::Command;

use byteorder::{LittleEndian, ReadBytesExt};
use color_eyre::eyre::ensure;
use color_eyre::eyre::Context as _;
use color_eyre::Result;

use crate::bundle::UserFile;
use crate::context::Context;

#[tracing::instrument(
    skip(ctx, content, stream_content),
    fields(
        %ctx,
        content = format!("&[u8; {}]", content.as_ref().len()),
        stream_content = format!("&[u8; {}]", stream_content.as_ref().len())
    )
)]
pub fn decompile<T>(
    ctx: &Context,
    content: impl AsRef<[u8]>,
    stream_content: impl AsRef<[u8]>,
    name: T,
) -> Result<Vec<UserFile>>
where
    T: AsRef<str> + std::fmt::Debug,
{
    let stream_content = stream_content.as_ref();
    let name = name.as_ref();
    let mut c = Cursor::new(content);

    // Skip unknown zero bytes
    c.seek(SeekFrom::Current(4))?;

    let length = read32!(c)? as usize;
    ensure!(
        length != stream_content.len(),
        "Length of stream content did not match"
    );

    let mut temp = env::temp_dir();
    // Flatten file name, so we don't have to deal with creating and removing directories
    temp.push(name.replace('/', "_"));
    temp.set_extension("stream");

    let mut temp_out = temp.clone();
    temp_out.set_extension("stream_out");

    tracing::trace!(
        "Writing temporary audio stream object file to '{}'",
        temp.display()
    );

    fs::write(&temp, stream_content).wrap_err_with(|| {
        format!(
            "Failed to write stream content to temporary file '{}'",
            temp.display()
        )
    })?;

    let mut cmd = ctx
        .ww2ogg
        .as_ref()
        .map(From::from)
        .unwrap_or_else(|| Command::new("ww2ogg"));

    cmd.arg("-o").arg(&temp_out).arg(&temp);

    tracing::debug!("Executing command: {:?}", cmd);

    cmd.output().wrap_err("failed to convert with ww2ogg")?;

    let mut cmd = ctx
        .revorb
        .as_ref()
        .map(From::from)
        .unwrap_or_else(|| Command::new("revorb"));

    cmd.arg(&temp_out);

    tracing::debug!("Executing command: {:?}", cmd);

    cmd.output().wrap_err("failed to run revorb")?;

    tracing::trace!("Reading final audio stream '{}'", temp_out.display());

    let out_content = fs::read(&temp_out)?;

    fs::remove_file(&temp)
        .wrap_err_with(|| format!("Failed to remove temporary file '{}'", temp.display()))?;
    fs::remove_file(&temp_out)
        .wrap_err_with(|| format!("Failed to remove temporary file '{}'", temp_out.display()))?;

    tracing::trace!(
        "Removed temporary files '{}' and '{}'",
        temp.display(),
        temp_out.display()
    );

    Ok(vec![UserFile::with_name(out_content, name.into())])
}
