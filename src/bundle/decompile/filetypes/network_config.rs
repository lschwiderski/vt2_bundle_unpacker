// Copyright (C) 2019-2021  Lucas Schwiderski
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
use std::collections::HashMap;
use std::fmt;
use std::io::{Cursor, Read, Seek, SeekFrom};

use byteorder::{LittleEndian, ReadBytesExt};
use color_eyre::eyre::{bail, eyre};
use color_eyre::Result;

use crate::bundle::UserFile;
use crate::context::Context;
use crate::murmur::{self, HashGroup, Murmur32};

#[derive(Debug)]
enum Primitive {
    Bool,
    Int,
    Int64,
    Float,
    Vector3,
    Array,
    Quaternion,
    String,
    GameObjectId,
    Unknown(u8),
}

impl Primitive {
    fn parse(i: u8, version: u32) -> Primitive {
        if version < 0x25 {
            match i {
                0x00 => Primitive::Bool,
                0x01 => Primitive::Int,
                0x02 => Primitive::Float,
                0x03 => Primitive::Vector3,
                0x04 => Primitive::Quaternion,
                0x05 => Primitive::String,
                0x07 => Primitive::Int64,
                0x08 => Primitive::Array,
                0x34 => Primitive::GameObjectId,
                _ => Primitive::Unknown(i),
            }
        } else {
            match i {
                0x00 => Primitive::Bool,
                0x01 => Primitive::Int,
                0x03 => Primitive::Float,
                0x04 => Primitive::Vector3,
                0x05 => Primitive::Quaternion,
                0x06 => Primitive::String,
                0x08 => Primitive::Int64,
                0x09 => Primitive::Array,
                0x34 => Primitive::GameObjectId,
                _ => Primitive::Unknown(i),
            }
        }
    }
}

impl fmt::Display for Primitive {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Bool => write!(f, "bool"),
            Self::Int => write!(f, "int"),
            Self::Float => write!(f, "float"),
            Self::Int64 => write!(f, "int64"),
            Self::Vector3 => write!(f, "vector3"),
            Self::String => write!(f, "string"),
            Self::Quaternion => write!(f, "quaternion"),
            Self::Array => write!(f, "array"),
            Self::GameObjectId => write!(f, "game_object_id"),
            Self::Unknown(i) => write!(f, "Unknown(0x{:02X})", i),
        }
    }
}

#[derive(Debug)]
struct TypeData {
    name: String,
    primitive: Primitive,
    bits: u16,
    data_1: u32,
    data_2: u32,
    data_3: u32,
}

#[inline]
fn read_short_hash<T>(ctx: &Context, c: &mut T) -> Result<String>
where
    T: Read,
{
    let hash = read32!(c)?;
    let name = ctx
        .dictionary
        .lookup_short(hash.into(), HashGroup::NetworkConfig)
        .cloned()
        .unwrap_or_else(|| format!("{:08X}", hash));
    Ok(name)
}

#[tracing::instrument(skip(ctx, binary))]
pub fn decompile(ctx: &Context, binary: impl AsRef<[u8]>) -> Result<Vec<UserFile>> {
    let mut content = String::with_capacity(binary.as_ref().len());
    let mut c = Cursor::new(binary);

    // Skip header bits
    let version = read32!(c)?;
    // Versions 0x24 & 0x25 don't seem to have any noticable difference over 0x23
    // in the the game's `.network_config`.
    if !(version == 0x20 || version == 0x23 || version == 0x24 || version == 0x25) {
        bail!(
            "Unsupported version detected: {} (0x{:X})",
            version,
            version
        );
    }

    if version == 0x20 {
        match read32!(c)? {
            0 => content.push_str("network_type=\"peer-to-peer\"\n\n"),
            1 => content.push_str("network_type=\"client-server\"\n\n"),
            network_type => bail!("Unknown network_type: {:#X}", network_type),
        }
    }

    content.push_str(&format!(
        "reliable_send_buffer_size={}\nreliable_receive_buffer_size={}",
        read32!(c)?,
        read32!(c)?
    ));

    let num_types = read32!(c)? as usize;
    let mut type_list: Vec<TypeData> = Vec::with_capacity(num_types);

    // Array types point to their sub-element type via index, which may be a type defined after them
    // So we need to iterate twice: once to read the data, once to write it
    for _ in 0..num_types {
        let t = if version == 0x20 {
            let primitive = Primitive::parse(read8!(c)?, version);
            let bits = read16!(c)?;
            let _padding = read32!(c)?;
            let name = read_short_hash(ctx, &mut c)?;
            let data_1 = read32!(c)?;
            let data_2 = read32!(c)?;
            let data_3 = read32!(c)?;

            TypeData {
                name,
                primitive,
                bits,
                data_1,
                data_2,
                data_3,
            }
        } else {
            let name = read_short_hash(ctx, &mut c)?;
            let _padding = read32!(c)?;
            let data_1 = read32!(c)?;
            let primitive = Primitive::parse(read8!(c)?, version);
            let bits = read16!(c)?;
            let _unknown_1 = read8!(c)?;
            let data_2 = read32!(c)?;
            let data_3 = read32!(c)?;

            TypeData {
                name,
                primitive,
                bits,
                data_1,
                data_2,
                data_3,
            }
        };

        type_list.push(t);
    }

    content.push_str("\n\ntypes = {");

    for t in type_list.iter() {
        content.push_str(&format!("\n\t{} = {{ type = \"{}\"", t.name, t.primitive));
        match t.primitive {
            // These types carry no extra data, so we don't need to write anything here
            // TODO: Doesn't String have a length/size property?
            Primitive::Bool | Primitive::String => {}
            Primitive::Int | Primitive::Int64 => {
                let min = i32::from_le_bytes(t.data_2.to_le_bytes());
                let max = i32::from_le_bytes(t.data_3.to_le_bytes());
                let is_default = min == 0 && max == 0 && t.bits == 0;

                // Rust doesn't have early returns from `match` branches, so we'll have to go nested
                if !is_default {
                    // Technically we could still skip writing `min = 0`, but we'll keep it to be explicit.
                    content.push_str(&format!(", min = {}", min));

                    // In all cases known so far, when `max` is set to `0` in the compiled file, the value wasn't specified in the source, but inferred from `min` and `bits`.
                    if max == 0 {
                        content.push_str(&format!(
                            ", max = {}",
                            i64::pow(2, t.bits as u32) - 1 + min as i64
                        ));
                    } else {
                        content.push_str(&format!(", max = {}", max));
                    }
                }
            }
            Primitive::Float | Primitive::Vector3 => {
                let min = f32::from_bits(t.data_2);
                let max = f32::from_bits(t.data_3);
                let tolerance = f32::from_bits(t.data_1);
                let default_tolerance = (1.0 / u32::pow(2, 1 + t.bits as u32) as f32) * (max - min);

                content.push_str(&format!(", min = {}, max = {}", min, max));

                if t.bits != 7 {
                    content.push_str(&format!(", bits = {}", t.bits));
                }

                if (tolerance - default_tolerance).abs() > f32::EPSILON {
                    content.push_str(&format!(", tolerance = {}", tolerance));
                }
            }
            Primitive::Quaternion => {
                let tolerance = f32::from_bits(t.data_1);
                // I don't actually know why the formula works the way it does. I just figured it out from looking at compiled values.
                let default_tolerance = {
                    // A Quaternion has four fields, but apparently it's only `3` here.
                    // Because of that, I'm also not sure if the name `bits_per_field` actually fits.
                    let num_fields = 3;
                    // A different way to write the formula would be: `max(ceil((t.bits - 1) / 3), 1)`.
                    // A bit harder to read, but maybe easier to comprehend.
                    // The formula below leverages the built-in `floor` of integer division.
                    let bits_per_field = (u32::saturating_sub(t.bits.into(), 2) / num_fields) + 1;
                    (1.0 / u32::pow(2, 1 + bits_per_field) as f32) * num_fields as f32
                };

                if t.bits != 32 {
                    content.push_str(&format!(", bits = {}", t.bits));
                }

                if (tolerance - default_tolerance).abs() > f32::EPSILON {
                    content.push_str(&format!(", tolerance = {}", tolerance));
                }
            }
            Primitive::Array => {
                let element = type_list.get(t.data_2 as usize);
                match element {
                    Some(el) => content.push_str(&format!(
                        ", max_size = {}, element = \"{}\"",
                        t.data_3, el.name
                    )),
                    None => bail!("couldn't find sub element at index {}", t.data_2),
                };
            }
            Primitive::Unknown(_) | Primitive::GameObjectId => {
                if cfg!(debug_assertions) {
                    tracing::warn!("Unknown primitive network type: {:?}", t.primitive);
                    content.push_str(&format!(
                        ", bits = {:b}, data_1 = {}, data_2 = {}, data_3 = {}",
                        t.bits, t.data_1, t.data_2, t.data_3
                    ));
                } else {
                    bail!(
                        "unexpected primitive network type during decompilation: {}",
                        t.primitive
                    )
                }
            }
        };
        content.push_str(" }");
    }
    content.push_str("\n}\n");

    let num_object_names = read32!(c)? as usize;
    let mut object_name_start_positions: Vec<u16> = Vec::with_capacity(num_object_names);
    let mut object_names: HashMap<Murmur32, String> = HashMap::with_capacity(num_object_names);

    for _ in 0..num_object_names {
        object_name_start_positions.push(read16!(c)?);
    }

    let total_names_length = read32!(c)? as u64;
    let names_start_position = c.position();

    for i in 0..num_object_names {
        let start = object_name_start_positions[i] as u64;
        if c.position() != (start + names_start_position) {
            bail!("Unexpected read position {}", c.position());
        }

        let end = object_name_start_positions
            .get(i + 1)
            .map(|n| *n as u64)
            .unwrap_or(total_names_length);

        // The file contains zero-terminated strings. So we read length-1 bytes and skip the terminator
        let mut buf = vec![0u8; (end - start - 1) as usize];
        c.read_exact(&mut buf)?;
        c.seek(SeekFrom::Current(1))?;

        let name = String::from_utf8(buf)?;
        let name_hash = Murmur32::from(murmur::hash32(name.as_bytes(), murmur::SEED));
        object_names.insert(name_hash, name);
    }

    // Skip some padding. Not sure why this is in the file.
    {
        let padding_length = read32!(c)?;
        c.seek(SeekFrom::Current(padding_length as i64))?;
    }

    let num_objects = read32!(c)? as usize;
    if num_objects != num_object_names {
        bail!(
            "number of objects ({}), doesn't match number of object names ({})",
            num_objects,
            num_object_names
        );
    }

    content.push_str("\nobjects = {");

    for _ in 0..num_objects {
        let name_hash = Murmur32::from(read32!(c)?);
        let name = match object_names.get(&name_hash) {
            Some(name) => name.clone(),
            None => {
                tracing::warn!(
                    "object name hash {:08X} not found in list of object names",
                    name_hash
                );
                format!("{:08X}", name_hash)
            }
        };
        // These two fields are not specified in the original file from FS.
        // So most likely generated out of the rest of the object definition.
        let _unknown_1 = read32!(c)?;
        let _unknown_2 = read32!(c)?;
        let priority = readf32!(c)?;
        let update_rate = readf32!(c)?;

        content.push_str(&format!(
            "\n\t{} = {{\n\t\tpriority = {}\n\t\tupdate_rate = {}\n\t\tfields = [",
            name, priority, update_rate
        ));

        // The number of bytes for all following values.
        // This exists, so we don't have magic numbers in the following code sections.
        let bytes = 4;

        // Object field data is split into two lists:
        // - field types as indices into the list of types defined earlier in the file
        // - field names
        // Before each list there is a uint32 containing the size of the list. They should always be equal.
        // We'll verify this early, by seeking to the appropriate place.
        let num_field_types = read32!(c)? as i64;
        let position = c.position();
        // Seek to next list size
        c.seek(SeekFrom::Current(num_field_types * bytes))?;
        let num_field_names = read32!(c)? as i64;
        // Go back to start of first list
        c.set_position(position);

        if num_field_types != num_field_names {
            bail!(
                "number of field types and field names for object doesn't match. don't know how to proceed"
            );
        }

        for _ in 0..num_field_types {
            let type_index = read32!(c)? as usize;

            // Seek to field name.
            // Technically, this would be ((num_field_types - 1) * sizeof(type_index) + sizeof(num_field_types)). Since all sizes are equal, we can simplify the equation.
            let offset = num_field_types * bytes;
            c.seek(SeekFrom::Current(offset))?;

            let name = read_short_hash(ctx, &mut c)?;
            // Seek back.
            c.seek(SeekFrom::Current(-(num_field_types + 1) * bytes))?;

            let t = &type_list[type_index];

            content.push_str(&format!(
                "\n\t\t\t{{ name = \"{}\", type = \"{}\" }}",
                name, t.name
            ));
        }

        // Seek to end of object data.
        // Technically, this would be (num_field_types * sizeof(type_name) + sizeof(num_field_name)). Since all sizes are equal, we can simplify the equation.
        c.seek(SeekFrom::Current((num_field_names + 1) * bytes))?;

        content.push_str("\n\t\t]\n\t}");
    }

    content.push_str("\n}");

    let num_messages = read32!(c)? as usize;

    content.push_str("\n\nmessages = {");

    for _ in 0..num_messages {
        // Skip name hash. We get the ASCII name later
        c.seek(SeekFrom::Current(4))?;
        let unknown = read32!(c)?;
        let session_bound = match read16!(c)? {
            0x0 => false,
            0x100 => true,
            val => {
                bail!("unknown value for 'session_bound': {}", val);
            }
        };

        // Each message name is always stored in 64 bits. If the content is smaller, it's padded with zeroes.
        let mut buf = vec![0u8; 64];
        c.read_exact(&mut buf)?;
        let name: String = buf
            .iter()
            .take_while(|&&c| c != 0u8)
            .map(|&c| char::from(c))
            .collect();

        if unknown != 0x1 {
            tracing::error!(
                "unexpected value for field in message '{}'. expected 0x1, got {:#X}\n",
                name,
                unknown
            );
        }

        content.push_str(&format!(
            "\n\t{} = {{\n\t\tsession_bound = {}\n\t\targs = [",
            name, session_bound
        ));

        let num_arguments = read32!(c)? as usize;
        for _ in 0..num_arguments {
            let type_index = read32!(c)? as usize;
            let t = type_list
                .get(type_index)
                .ok_or_else(|| eyre!("Couldn't find type with index {}", type_index))?;

            content.push_str(&format!("\n\t\t\t\"{}\"", t.name));
        }

        content.push_str("\n\t\t]\n\t}");
    }

    content.push_str("\n}");

    Ok(vec![UserFile::new(content.into_bytes())])
}
