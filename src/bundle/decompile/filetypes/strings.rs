// Copyright (C) 2019-2021  Lucas Schwiderski
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
use std::io::{Cursor, Read};

use byteorder::{LittleEndian, ReadBytesExt};
use color_eyre::{Report, Result};

use crate::bundle::UserFile;
use crate::context::Context;
use crate::murmur::HashGroup;

#[inline]
fn read_short_hash<T: Read>(ctx: &Context, c: &mut T) -> Result<String> {
    let hash = read32!(c)?;
    let s = ctx
        .dictionary
        .lookup_short(hash.into(), HashGroup::Strings)
        .cloned()
        .unwrap_or_else(|| format!("{:08X}", hash));
    Ok(s)
}

#[inline]
fn read_string<T>(c: T) -> Result<String>
where
    T: Read,
{
    c.bytes()
        // Take up to the first error or first `0`
        .take_while(|b| b.is_ok() && *b.as_ref().unwrap() != 0)
        .map(|b| b.map_err(Report::from))
        .collect::<Result<_>>()
        .and_then(|bytes| String::from_utf8(bytes).map_err(Report::from))
}

#[tracing::instrument(skip(ctx, binary))]
pub fn decompile(ctx: &Context, binary: impl AsRef<[u8]>) -> Result<Vec<UserFile>> {
    let mut content = String::with_capacity(binary.as_ref().len());
    let mut c = Cursor::new(binary);

    let _header = read32!(c)?;
    let count = read32!(c)? as usize;

    for _ in 0..count {
        let name = read_short_hash(ctx, &mut c)?;
        let address = read32!(c)? as u64;

        let pos = c.position();
        c.set_position(address);
        let s = read_string(&mut c)?;

        c.set_position(pos);

        content.push_str(&format!("{} = {{\n\ten = \"{}\"\n}}\n", name, s));
    }

    Ok(vec![UserFile::new(content.into_bytes())])
}
