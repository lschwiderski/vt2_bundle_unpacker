// Copyright (C) 2019-2021  Lucas Schwiderski
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
use std::collections::HashMap;
use std::fmt;
use std::io::{Cursor, Read, Seek, SeekFrom};

use byteorder::{LittleEndian, ReadBytesExt};
use color_eyre::eyre::{bail, Context as _};
use color_eyre::Result;

use super::wem::WEMFile;

mod data;
mod hirc;

const BKHD_HEADER: u32 = 0x44484b42;

#[derive(PartialEq, Eq, Hash, Copy, Clone)]
enum SectionType {
    Didx,
    Data,
    Hirc,
    Stid,
    Stmg,
    Envs,
    Fxpr,
    Unknown(u32),
}

impl fmt::Display for SectionType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Didx => write!(f, "DIDX"),
            Self::Data => write!(f, "DATA"),
            Self::Hirc => write!(f, "HIRC"),
            Self::Stid => write!(f, "STID"),
            Self::Stmg => write!(f, "STMG"),
            Self::Envs => write!(f, "ENVS"),
            Self::Fxpr => write!(f, "FXPR"),
            Self::Unknown(t) => write!(f, "Unknown({:08X})", t),
        }
    }
}

impl From<u32> for SectionType {
    fn from(t: u32) -> Self {
        match t {
            0x58444944 => Self::Didx,
            0x41544144 => Self::Data,
            0x43524948 => Self::Hirc,
            0x44495453 => Self::Stid,
            0x474d5453 => Self::Stmg,
            0x53564e45 => Self::Envs,
            0x52505846 => Self::Fxpr,
            _ => Self::Unknown(t),
        }
    }
}

macro_rules! check_header {
    ($c:expr, $h:expr) => {{
        let header = read32!($c)?;
        if header != $h {
            bail!(
                "Unexpected header. Expected {:08X}, got {:08X}. At position {}",
                $h,
                header,
                $c.position()
            );
        }
    }};
}

#[tracing::instrument(skip(content))]
pub fn extract_files(content: impl AsRef<[u8]>) -> Result<Vec<WEMFile>> {
    let content_length = content.as_ref().len();
    let mut c = Cursor::new(content);

    check_header!(c, BKHD_HEADER);

    // Wrap in a scope, so we keep the variable names in the parent scope clean
    {
        let section_length = read32!(c)? as usize;
        // Skip `version`, `bnk_id` and a bunch of unknown fields
        // We don't need any of that
        c.seek(SeekFrom::Current(section_length as i64))?;
    }

    let bank_size = content_length as u64 - c.position();

    // Each bank may contain some or all of various section types in any order
    // (but only one of each, AFAIK). So we read the file once and store
    // the sections we find, then handle them later.
    let section_positions: HashMap<SectionType, u64> = {
        let mut section_positions: HashMap<SectionType, u64> = HashMap::new();
        let mut seeked_size = c.position();

        while seeked_size < bank_size {
            let pos = c.position();
            let header = SectionType::from(read32!(c)?);

            // Record section id and position
            section_positions.insert(header, pos);

            let size = read32!(c)? as u64;

            tracing::trace!("Found section '{}' at {} with size {}", header, pos, size);

            // Seek to next section
            c.seek(SeekFrom::Current(size as i64))?;
            seeked_size += 8 + size;
        }

        c.seek(SeekFrom::Start(0))?;

        section_positions
    };

    if let Some(didx_pos) = section_positions.get(&SectionType::Didx) {
        tracing::debug!("Found DIDX section at {}", didx_pos);

        let data_pos = match section_positions.get(&SectionType::Data) {
            Some(pos) => pos,
            None => {
                bail!("Found DIDX section but no DATA section. Don't know how to handle this case")
            }
        };

        let mut didx_buf = {
            // Skip header field
            c.seek(SeekFrom::Start(*didx_pos + 4))?;
            let length = read32!(c)? as usize;
            let mut buf = vec![0u8; length];
            c.read_exact(&mut buf)?;
            buf
        };

        let mut data_buf = {
            // Skip header field
            c.seek(SeekFrom::Start(*data_pos + 4))?;
            let length = read32!(c)? as usize;
            let mut buf = vec![0u8; length];
            c.read_exact(&mut buf)?;
            buf
        };

        data::extract_data_files(&mut didx_buf, &mut data_buf)
            .wrap_err("Failed to extract DATA section")
    } else if let Some(hirc_pos) = section_positions.get(&SectionType::Hirc) {
        let mut buf = {
            // Skip header field
            c.seek(SeekFrom::Start(*hirc_pos + 4))?;
            let length = read32!(c)? as usize;
            let mut buf = vec![0u8; length];
            c.read_exact(&mut buf)?;
            buf
        };

        hirc::extract_hirc_files(&mut buf).wrap_err("Failed to extract HIRC section")
    } else {
        bail!("No meaningful sections found");
    }
}
