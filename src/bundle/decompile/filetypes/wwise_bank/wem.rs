// Copyright (C) 2019-2021  Lucas Schwiderski
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::collections::HashMap;
use std::io::{Cursor, Seek, SeekFrom, Write};

use byteorder::{LittleEndian, ReadBytesExt, WriteBytesExt};
use color_eyre::eyre::bail;
use color_eyre::Result;

const RIFF_HEADER: u32 = 0x46464952;
const WAVE_HEADER: u32 = 0x45564157;
const FMT_HEADER: u32 = 0x20746d66;
const DATA_HEADER: u32 = 0x61746164;

pub struct Fmt {
    audio_format: u16,
    num_channels: usize,
    sample_rate: u32,
    byte_rate: u32,
    block_size: usize,
    bits_per_sample: u16,
}

pub struct WEMFile {
    id: u32,
    content: Vec<u8>,
}

impl WEMFile {
    pub fn new(id: u32, content: Vec<u8>) -> Self {
        WEMFile { id, content }
    }

    #[allow(dead_code)]
    pub fn get_content(&self) -> &[u8] {
        &self.content
    }

    pub fn to_wav(&self) -> Result<Vec<u8>> {
        ima_adpcm_to_pcm(&self.content)
    }

    pub fn get_name(&self) -> String {
        format!("{}", self.id)
    }
}

const IMA_STEPS: [i16; 89] = [
    7, 8, 9, 10, 11, 12, 13, 14, 16, 17, 19, 21, 23, 25, 28, 31, 34, 37, 41, 45, 50, 55, 60, 66,
    73, 80, 88, 97, 107, 118, 130, 143, 157, 173, 190, 209, 230, 253, 279, 307, 337, 371, 408, 449,
    494, 544, 598, 658, 724, 796, 876, 963, 1060, 1166, 1282, 1411, 1552, 1707, 1878, 2066, 2272,
    2499, 2749, 3024, 3327, 3660, 4026, 4428, 4871, 5358, 5894, 6484, 7132, 7845, 8630, 9493,
    10442, 11487, 12635, 13899, 15289, 16818, 18500, 20350, 22385, 24623, 27086, 29794, 32767,
];
const IMA_INDICES: [i8; 16] = [-1, -1, -1, -1, 2, 4, 6, 8, -1, -1, -1, -1, 2, 4, 6, 8];

fn decode_sample(sample: u8, previous_sample: i16, step: i16) -> i16 {
    let delta = sample & 7;
    let mut difference = step >> 3;

    if (delta & 4) == 4 {
        difference = difference.saturating_add(step);
    }

    if (delta & 2) == 2 {
        difference = difference.saturating_add(step >> 1);
    }

    if (delta & 1) == 1 {
        difference = difference.saturating_add(step >> 2);
    }

    if (sample & 8) == 8 {
        difference = difference.saturating_mul(-1);
    }

    difference.saturating_add(previous_sample)
}

fn next_step_index(sample: u8, previous_step_index: usize) -> usize {
    let next_index =
        (previous_step_index as isize).saturating_add(IMA_INDICES[sample as usize] as isize);

    if next_index >= 88 {
        88
    } else if next_index <= 0 {
        0
    } else {
        next_index as usize
    }
}

#[tracing::instrument(skip(c))]
fn decode_ima_adpcm(
    c: &mut Cursor<impl AsRef<[u8]>>,
    output_buffer: &mut [i16],
    channel_index: usize,
    num_channels: usize,
) -> Result<()> {
    let mut sample_number: usize = 0;
    let mut sample: u8 = 0;

    // Initial values
    let seed_sample = readi16!(c)?;
    let seed_step_index = read8!(c)? as usize;

    // Skip alignment byte
    c.seek(SeekFrom::Current(1))?;

    let mut previous_sample: i16 = seed_sample;
    let mut previous_step_index = seed_step_index;

    output_buffer[(num_channels * sample_number) + channel_index] = seed_sample;
    sample_number += 1;

    // TODO: Refactor magic number
    for i in 1..64 {
        // Alternate between first and second branch, starting with the first
        if (i % 2) == 1 {
            sample = read8!(c)?;
            // `sample & 0xf`: Choose bits 0-3 (least significant 4 bits)
            previous_sample = decode_sample(
                sample & 0xf,
                previous_sample,
                IMA_STEPS[previous_step_index],
            );
            previous_step_index = next_step_index(sample & 0xf, previous_step_index);
        } else {
            // `sample >> 4`: Choose bits 4-7 (most significant bits)
            previous_sample =
                decode_sample(sample >> 4, previous_sample, IMA_STEPS[previous_step_index]);
            previous_step_index = next_step_index(sample >> 4, previous_step_index);
        }

        output_buffer[(num_channels * sample_number) + channel_index] = previous_sample;
        sample_number += 1;
    }

    Ok(())
}

#[tracing::instrument(skip(content))]
fn ima_adpcm_to_pcm(content: impl AsRef<[u8]>) -> Result<Vec<u8>> {
    let mut c = Cursor::new(content);

    let chunk_id = read32!(c)?;
    if chunk_id != RIFF_HEADER {
        bail!(
            "Unexpected file format header. Expected 'RIFF' ({:08X}), got {:08X}",
            RIFF_HEADER,
            chunk_id
        );
    }

    // Size of the entire file minus the first two fields
    let riff_size = read32!(c)? as usize;

    let format = read32!(c)?;
    if format != WAVE_HEADER {
        bail!(
            "Unexpected format. Excepected 'WAVE' ({:08X}), got {:08X}",
            WAVE_HEADER,
            format
        )
    }

    // Some files contain various chunks with information that we don't care about
    // So we read the whole file once to find the positions of every chunk,
    // then pick the ones we want to read
    let chunk_positions: HashMap<u32, u64> = {
        let mut chunk_positions: HashMap<u32, u64> = HashMap::new();
        let mut seeked_size = c.position();

        while seeked_size < riff_size as u64 {
            let pos = c.position();
            let header = read32!(c)?;

            // Record chunk id and position
            chunk_positions.insert(header, pos);

            let size = {
                let size = read32!(c)? as u64;
                if size % 2 == 0 {
                    size
                } else {
                    size + 1
                }
            };

            tracing::trace!("Found chunk '{:08X}' at {} with size {}", header, pos, size,);

            // Seek to next chunk
            c.seek(SeekFrom::Current(size as i64))?;
            seeked_size += 8 + size;
        }

        c.seek(SeekFrom::Start(0))?;

        chunk_positions
    };

    let fmt: Fmt = {
        let chunk_position = match chunk_positions.get(&FMT_HEADER) {
            Some(pos) => pos,
            None => bail!("Couldn't find 'fmt ' chunk."),
        };

        // TODO: Make verbose == 2
        tracing::trace!("Found 'fmt' chunk at {}", chunk_position);

        // We already confirmed the chunk's header when collection chunk positions
        // and we don't need the size here
        c.seek(SeekFrom::Start(chunk_position + 8))?;

        let audio_format = read16!(c)?;
        if audio_format != 0x2 {
            bail!(
                "Unknown audio format. Expected IMA ADPCM (0x2), got {:X}",
                audio_format
            );
        }

        let num_channels = read16!(c)? as usize;
        let sample_rate = read32!(c)?;
        let byte_rate = read32!(c)?;
        let block_size = read16!(c)? as usize;
        let bits_per_sample = read16!(c)?;

        // Perform consistency checks
        if block_size % num_channels != 0 {
            bail!("Block size doesn't devide evenly by number of channels");
        }

        if (block_size / num_channels) % 4 != 0 {
            bail!("Bytes per channel doesn't devide evenly by 4.");
        }

        let extra_params_size = read16!(c)?;
        c.seek(SeekFrom::Current(extra_params_size as i64))?;

        Fmt {
            audio_format,
            num_channels,
            sample_rate,
            byte_rate,
            block_size,
            bits_per_sample,
        }
    };

    let decompiled_data: Vec<u8> = {
        let chunk_position = match chunk_positions.get(&DATA_HEADER) {
            Some(pos) => pos,
            None => bail!("Couldn't find 'fmt ' chunk."),
        };

        tracing::trace!("Found 'DATA' chunk at {}", chunk_position);

        // We already confirmed the chunk's header when collection chunk positions
        c.seek(SeekFrom::Start(chunk_position + 4))?;

        let size = read32!(c)? as usize;

        if size % fmt.block_size as usize != 0 {
            bail!("Data size doesn't devide evenly by block size.");
        }

        let mut decompiled: Vec<u8> = Vec::new();

        tracing::trace!("Decoding {} blocks of IMA ADPCM", size / fmt.block_size);
        for _ in 0..(size / fmt.block_size) {
            // TODO: Only initialize the buffer once
            let mut buf = vec![0i16; 64 * fmt.num_channels];

            for channel_index in 0..fmt.num_channels {
                decode_ima_adpcm(&mut c, &mut buf, channel_index, fmt.num_channels)?;
            }

            for sample in buf.iter() {
                for byte in &sample.to_le_bytes() {
                    decompiled.push(*byte);
                }
            }
        }

        decompiled
    };

    let out_fmt = Fmt {
        // 1 == PCM
        audio_format: 1,
        num_channels: fmt.num_channels,
        sample_rate: fmt.sample_rate,
        bits_per_sample: 16,
        // 2 == bits_per_sample / 8
        block_size: fmt.num_channels * 2,
        byte_rate: (fmt.num_channels as u32) * 2 * fmt.sample_rate,
    };

    let output: Vec<u8> = Vec::new();
    let mut c_out = Cursor::new(output);

    let data_size = decompiled_data.len() as u32;
    // Length of sample data plus header and size fields
    let data_chunk_size = data_size + 8;
    // For PCM the `fmt` chunk will be 16 bytes long
    // plus header and size fields
    let fmt_chunk_size = 24;

    write32!(c_out, RIFF_HEADER)?;
    // RIFF size
    // Add 4 bytes for the format header
    write32!(c_out, data_chunk_size + fmt_chunk_size + 4)?;
    write32!(c_out, WAVE_HEADER)?;
    write32!(c_out, FMT_HEADER)?;
    // 'fmt ' chunk data size
    write32!(c_out, 16)?;
    write16!(c_out, out_fmt.audio_format)?;
    write16!(c_out, out_fmt.num_channels as u16)?;
    write32!(c_out, out_fmt.sample_rate)?;
    write32!(c_out, out_fmt.byte_rate)?;
    write16!(c_out, out_fmt.block_size as u16)?;
    write16!(c_out, out_fmt.bits_per_sample)?;
    write32!(c_out, DATA_HEADER)?;
    write32!(c_out, data_size)?;
    c_out.write_all(&decompiled_data)?;

    Ok(c_out.into_inner())
}
