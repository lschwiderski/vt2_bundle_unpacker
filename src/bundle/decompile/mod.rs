// Copyright (C) 2019-2021  Lucas Schwiderski
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
//
pub mod filetypes {
    pub mod bones;
    pub mod lua;
    pub mod material;
    pub mod mod_file;
    pub mod network_config;
    pub mod package;
    pub mod strings;
    pub mod texture;
    pub mod wwise_bank;
    pub mod wwise_dep;
    pub mod wwise_stream;
}
