default: run

set positional-arguments := true
run *args='':
    cargo run -- "$@"

release:
    cargo build --release --locked

perf *args='':
    cargo build --profile perf
    perf record --call-graph dwarf ./target/perf/unpacker "$@"

list-content bundle_dir=env_var('bundle_dir'): release
    #!/bin/env bash
    export RUST_LOG="error"
    tmp=$(mktemp -d --suffix ".unpacker-log")
    N=6
    echo "$(tput bold)Reading bundles from '{{bundle_dir}}', temp dir is '$tmp'$(tput sgr0)" >&2
    /usr/bin/find "{{bundle_dir}}" -regextype sed -regex '.*/[a-f0-9]\{16\}$' | while IFS= read -r file; do
        (
            bundle_name=$(basename "$file")
            name=$(./target/release/unpacker murmur lookup "$bundle_name" 2>/dev/null)

            if [[ "$name" =~ "Unknown" ]] && [ -n "$name" ]; then
                echo "- Warhammer Vermintide 2/bundle/$bundle_name" >> "$tmp/$bundle_name.txt"
            else
                echo "- Warhammer Vermintide 2/bundle/$bundle_name: $name" >> "$tmp/$bundle_name.txt"
            fi

            echo "$(tput bold)Listing bundle $bundle_name$(tput sgr0)" >&2
            ./target/release/unpacker list "$file" | sort | awk '{print "\t" $0}' >> "$tmp/$bundle_name.txt"
            echo "" >> "$tmp/$bundle_name.txt"
        ) &

        if [[ $(jobs -r -p | wc -l) -ge $N ]]; then
            wait
        fi

    done

    wait

    # The above wait doesn't seem to be enough to consolidate
    # all jobs, so we'll do some sketchy sleeping as well.
    sleep 60

    echo "$(tput bold)Consolidating listings$(tput sgr0)" >&2
    /bin/cat $tmp/*.txt 2>/dev/null
    \rm -r "$tmp"
